# Sector Files
This is my personal repository for all the sector files I have worked on.
You may download and edit whatever you wish, but do not distrubute as your own work. Also, if you have a request for a sector file, please email me at prithvisagar.shivaraman@gmail.com.

### ASDE-X Files
ASDE-X Files require the color profile that is bundled with them. Make sure you import the color profiles so they look normal.
Also, ASDE-X functions the best on the ARTS radar mode

### Real World Use
As always, please do NOT use any of the files here for any real-world use. This is meant to be used ONLY for the Virtual Air Traffic Simulation Network.